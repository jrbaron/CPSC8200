from PyQt5.QtWidgets import QMainWindow, QApplication
from PyQt5.QtOpenGL import QGLWidget

import pyopencl as cl
import numpy as np

import sys
from time import time


#--------------------------------------------
#Hands-On OpenCL by Simon McIntosh-Smith and Tom Deakin
#https://github.com/HandsOnOpenCL/Exercises-Solutions/blob/master/Exercises/Exercise03/Python/vadd.py
def PyOpenCL_test():
    N = 4

    context = cl.create_some_context()
    #platforms = cl.get_platforms()

    queue = cl.CommandQueue(context)
    kernelSource = open('VecAdd.cl').read()
    program = cl.Program(context, kernelSource)
    program = program.build()

  #Host arrays
    host_A = np.random.rand(N).astype(np.float32)
    host_B = np.random.rand(N).astype(np.float32)
    host_C = np.empty(N).astype(np.float32)
    print("Vector A: ", host_A)
    print("Vector B: ", host_B)
    print("Vector C (before add): ", host_C)

  #Device buffers: Create input arrays in device memory with data copied from host mem.
    dev_A = cl.Buffer(context, cl.mem_flags.READ_ONLY | cl.mem_flags.COPY_HOST_PTR, hostbuf=host_A)
    dev_B = cl.Buffer(context, cl.mem_flags.READ_ONLY | cl.mem_flags.COPY_HOST_PTR, hostbuf=host_B)
  #Create output array in device mem.
    dev_C = cl.Buffer(context, cl.mem_flags.WRITE_ONLY, host_C.nbytes)

  #Start a timer.
    runtime = time()

  #Execute kernel over entire range of the 1D input.
  #OpenCL runtime is allowed to select the work group items for the device.
    vecAdd = program.vecAdd
    vecAdd.set_scalar_arg_dtypes( [None, None, None, np.uint32] )
    vecAdd(queue, host_A.shape, None, dev_A, dev_B, dev_C, N)

  #Wait for the commands to finish before reading back.
    queue.finish()
    runtime = time()-runtime
    print("Kernel runtime (seconds): ", runtime)

  #Read back results from the compute device.
    cl.enqueue_copy(queue, host_C, dev_C)
    print("Vector C (after add): ", host_C)

#--------------------------------------------------
PyOpenCL_test()

if __name__ == "__main__":
    app = QApplication(sys.argv)
    win = QMainWindow()
    win.setWindowTitle("HALLO, LIEBLING.")
    win.show()
    sys.exit(app.exec_())
