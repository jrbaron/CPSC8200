import numpy as np
import pyopencl as CL
from pyopencl.scan import GenericScanKernel, GenericDebugScanKernel

#ElementwiseKernel() for "mapping" to each element.
#ReductionKernel() takes scalar and vector args (at least one vector). Good for sums and counts.
#GenericKernelScan() "generates and executes code that performs prefix sums ('scans') on arbitrary types, with many possible tweaks."

#j for Jess! Global variables.
jPlatform = None
jDevice = None
jContext = None
jCmdQueue = None

#----------------------------------------
def setupCL():
    global jPlatform, jDevice, jCmdQueue, jContext

    #Longer way of creating a context vs. "create_some_context()."
    platforms = CL.get_platforms()
    devices = platforms[0].get_devices()
    jContext = CL.Context(
        dev_type=CL.device_type.ALL,
        properties=[ (CL.context_properties.PLATFORM, platforms[0]) ]
    )

    print("Platforms:")
    for pl in platforms:
        print("\t", pl.name)
    print("Devices:")
    for d in devices:
        print("\t", d.name)

    jPlatform = platforms[0]
    jDevice = devices[0]
    jCmdQueue = CL.CommandQueue(jContext)

#----------------------------------------
#https://documen.tician.de/pyopencl/algorithm.html
def runPrefixSum():
    #prefixSumKernel = GenericDebugScanKernel(   #Helps find bugs in the parallel setup.
    prefixSumKernel = GenericScanKernel(
        jContext, np.int32,
        arguments="__global int *ary",
        input_expr="ary[i]",
        scan_expr="a+b",
        neutral="0",
        output_statement="ary[i] = item;"   #A C-statement that writes the output of the scan.
        )                                     #  'item' and 'prev_item' are already in the PyOpenCL C-code.

    A = CL.array.arange(jCmdQueue, 10, dtype=np.int32)
    #dev_data = CL.Buffer(jContext, CL.mem_flags.READ_ONLY | CL.mem_flags.COPY_HOST_PTR, hostbuf=A)

    print("Array BEFORE prefix sum: ", A)
    prefixSumKernel(A, queue=jCmdQueue)
    print("Array AFTER prefix sum: ", A)

#----------------------------------------
if __name__ == "__main__":

    print("PyOpenCL compiled with OpenGL interoperability?", CL.have_gl())
    #PyOpenCL and GLUT animation example:  https://pythonhosted.org/glitter/examples/opencl.html

    setupCL()
    runPrefixSum()
