//http://www.amdahlsoftware.com/wp-content/uploads/2013/01/matmul_img_opt.cl



//Happens per letter (if numLetters < max work items), so transf. mats don't know of each other yet.
__kernel void setTransMats( __global char *moduleString,
            __write_only image2d_t transMats,
            uint numLetters,
            float fwdStride )
{

    uchar ch;
    uint lettersBy4, c_id, m_id;
    float4 row0, row1, row2, row3;

    lettersBy4 = numLetters*4;
    for (m_id=0; m_id<lettersBy4; m_id=m_id+4)
    {
        c_id = m_id/4;
        ch = moduleString[c_id];

        if (ch != 'F')
        {


        }

        else   //Identity if no local transform.
        {
            row0.x = 1.0;    row0.y = 0.0;    row0.z = 0.0;    row0.w = 0.0;
            row1.x = 0.0;    row1.y = 1.0;    row1.z = 0.0;    row1.w = 0.0;
            row2.x = 0.0;    row2.y = 0.0;    row2.z = 1.0;    row2.w = 0.0;
            row2.x = 0.0;    row2.y = 0.0;    row2.z = 0.0;    row2.w = 1.0;

        }
    }

}