import pyopencl as cl
import numpy as np
from time import time

#Matrix multiplication using OpenCL Images.
#http://www.amdahlsoftware.com/wp-content/uploads/2013/01/matmul_img_opt.cl

#===========================
def setMatricesForModule(module):
    context = cl.create_some_context()
    cmdQueue = cl.CommandQueue(context)

    host_matricesArr = np.empty(len(module)).astype(np.float32)

    dev_modLetters = cl.Buffer(context, cl.mem_flags.READ_ONLY | cl.mem_flags.COPY_HOST_PTR, hostbuf=module)
    dev_matricesArr = cl.Buffer(context, cl.mem_flags.WRITE_ONLY, hostbuf=host_matricesArr)

    kernelSource = open('matrices.cl').read()
    program = cl.Program(context, kernelSource)
    program = program.build()


    runtime = time()

    kernel_setTransMat = program.setTransMats()

    runtime = time()-runtime

#===========================
def printClInfo():
    print("Max work group size:", cl.device_info.MAX_WORK_GROUP_SIZE)
    print("Max work item dimensions:", cl.device_info.MAX_WORK_ITEM_DIMENSIONS)
    print("Max work item sizes:", cl.device_info.MAX_WORK_ITEM_SIZES)

#===========================
printClInfo()
setMatricesForModule("F+F")
