

__kernel void vecAdd( __global float *A, __global float *B, __global float *C, const unsigned int count)
{
    int id = get_global_id(0);
    if (id < count)
    {
        C[id] = A[id] + B[id];
    }
}