import sys, os, datetime
from PyQt5.QtWidgets import QApplication
from PyQt5.QtGui import QSurfaceFormat

viewPath = os.path.abspath(os.path.dirname(__file__))+"/view/"
modelPath = os.path.abspath(os.path.dirname(__file__))+"/model/"
sys.path.append(viewPath)
sys.path.append(modelPath)

from MainWindow import MainWindow
from LSystemGenerator import LSystemGenerator
import numpy as np

if __name__ == "__main__":

  #Setting the QSurfaceFormat before the application creation might be needed for Mac OS.
    glFormat = QSurfaceFormat()
    glFormat.setSwapBehavior(QSurfaceFormat.DoubleBuffer)
    glFormat.setSwapInterval(1)
    glFormat.setVersion(4, 1)
    glFormat.setProfile(QSurfaceFormat.CoreProfile)
    glFormat.setSamples(4)
    QSurfaceFormat.setDefaultFormat(glFormat)

    app = QApplication(sys.argv)  # Perhaps SurfaceFormat after creation but before exec()?

    if sys.argv[1] == "help":
        print("CALL THIS: python main.py <string fractalName> <int depth> <bool useOpenCL> <float fwdStep> <float rotationRad>")


    name = "KochIsland"
    depth = 0
    useOpenCL = True
    fwd = 0.1
    rad = np.pi / 2.0

    if len(sys.argv) >= 4:
        print("Using command line arguments: ", sys.argv)
        name = sys.argv[1]
        depth = int(sys.argv[2])
        cl = sys.argv[3]
        if cl == 'True':
            useOpenCL = True
        else:
            useOpenCL = False

        if len(sys.argv) == 6:
            fwd = float(sys.argv[4])
            rad = float(sys.argv[5])

    else:
        print("Insufficient command line arguments.  Using defaults.")

    gen = LSystemGenerator(name, depth, useOpenCL, fwd, rad)

    now = datetime.datetime.now()
    tm = now.strftime("%Y-%m-%d %H:%M")

    if useOpenCL == False:
        logfile = open("RuntimeLogs/NoOpenCL/RuntimeLog_"+str(now)+".txt", 'w')
    else:
        logfile = open("RuntimeLogs/OpenCL/RuntimeLog_"+str(now)+".txt", 'w')

    win = MainWindow(gen, logfile)
    logfile.close()

    sys.exit(app.exec_())