L-System Generator Logfile
Created on 2017-12-04 19:15:03.821065
Author: Jessica Baron

L-System Attributes:
	Fractal Name: 				KochIsland
	Derivation Depth: 			0
	Line segment length: 		0.1
	Rotation angle (radians): 	1.570795
	Using OpenCL kernels? 		False
	Axiom String: 				A+A+A+A

Runtimes:
Interpretation Runtimes (Milliseconds):
		1.  Setting local matrices: 		0.39124488830566406

Total Runtimes (Milliseconds):
	Derivation: 		0.19860267639160156
	Interpretation: 	0.7028579711914062