L-System Generator Logfile
Created on 2017-12-06 21:05:39.073166
Author: Jessica Baron

L-System Attributes:
	Fractal Name: 				KochIsland
	Derivation Depth: 			4
	Line segment length: 		0.1
	Rotation angle (radians): 	1.570795
	Using OpenCL kernels? 		False
	Axiom String: 				A+A+A+A

Runtimes:
	Depth: 			0
	String: 		A+A+A+A
	String length: 	7
	Derivation Kernel/Function Runtimes (Milliseconds):
		1.  Successor counting: 			0.007152557373046875
		2.  Scan/prefix-sum of counts: 		0.020503997802734375
		3.  Rewriting: 						0.015020370483398438


	Depth: 			1
	String: 		A+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A
	String length: 	59
	Derivation Kernel/Function Runtimes (Milliseconds):
		1.  Successor counting: 			0.01811981201171875
		2.  Scan/prefix-sum of counts: 		0.016450881958007812
		3.  Rewriting: 						0.07963180541992188


	Depth: 			2
	String: 		A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A-A+A-A-AA+A+A-AA+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A-A+A-A-AA+A+A-AA+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A-A+A-A-AA+A+A-AA+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A-A+A-A-AA+A+A-AA+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A
	String length: 	475
	Derivation Kernel/Function Runtimes (Milliseconds):
		1.  Successor counting: 			0.08440017700195312
		2.  Scan/prefix-sum of counts: 		0.015020370483398438
		3.  Rewriting: 						0.5810260772705078


	Depth: 			3
	String: 		A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A-A+A-A-AA+A+A-AA+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A-A+A-A-AA+A+A-AA+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A-A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A-A+A-A-AA+A+A-AA+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A-A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A-A+A-A-AA+A+A-AA+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-AA+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A-A+A-A-AA+A+A-AA+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A-A+A-A-AA+A+A-AA+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A-A+A-A-AA+A+A-AA+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A-A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A-A+A-A-AA+A+A-AA+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A-A+A-A-AA+A+A-AA+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A-A+A-A-AA+A+A-AA+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A-A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A-A+A-A-AA+A+A-AA+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A-A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A-A+A-A-AA+A+A-AA+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-AA+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A-A+A-A-AA+A+A-AA+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A-A+A-A-AA+A+A-AA+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A-A+A-A-AA+A+A-AA+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A-A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A-A+A-A-AA+A+A-AA+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A-A+A-A-AA+A+A-AA+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A-A+A-A-AA+A+A-AA+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A-A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A-A+A-A-AA+A+A-AA+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A-A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A-A+A-A-AA+A+A-AA+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-AA+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A-A+A-A-AA+A+A-AA+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A-A+A-A-AA+A+A-AA+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A-A+A-A-AA+A+A-AA+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A-A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A-A+A-A-AA+A+A-AA+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A-A+A-A-AA+A+A-AA+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A-A+A-A-AA+A+A-AA+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A-A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A-A+A-A-AA+A+A-AA+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A-A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A-A+A-A-AA+A+A-AA+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-AA+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A-A+A-A-AA+A+A-AA+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A-A+A-A-AA+A+A-AA+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A-A+A-A-AA+A+A-AA+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A-A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A-A+A-A-AA+A+A-AA+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A
	String length: 	3803
	Derivation Kernel/Function Runtimes (Milliseconds):
		1.  Successor counting: 			0.7064342498779297
		2.  Scan/prefix-sum of counts: 		0.020265579223632812
		3.  Rewriting: 						4.631280899047852


Interpretation Runtimes (Milliseconds):
		1.  Setting local matrices: 		89.73145484924316

Total Runtimes (Milliseconds):
	Derivation: 		158.73456001281738
	Interpretation: 	274.5077610015869