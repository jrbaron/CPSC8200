L-System Generator Logfile
Created on 2017-12-07 00:16:34.328621
Author: Jessica Baron

L-System Attributes:
	Fractal Name: 				KochIsland
	Derivation Depth: 			7
	Line segment length: 		0.1
	Rotation angle (radians): 	1.570795
	Using OpenCL kernels? 		False
	Axiom String: 				A+A+A+A

Runtimes:
	Depth: 			0
	String length: 	7
	Derivation Kernel/Function Runtimes (Milliseconds):
		1.  Successor counting: 			0.015974044799804688
		2.  Scan/prefix-sum of counts: 		0.051021575927734375
		3.  Rewriting: 						0.028133392333984375


	Depth: 			1
	String length: 	59
	Derivation Kernel/Function Runtimes (Milliseconds):
		1.  Successor counting: 			0.031948089599609375
		2.  Scan/prefix-sum of counts: 		0.02193450927734375
		3.  Rewriting: 						0.11014938354492188


	Depth: 			2
	String length: 	475
	Derivation Kernel/Function Runtimes (Milliseconds):
		1.  Successor counting: 			0.13899803161621094
		2.  Scan/prefix-sum of counts: 		0.023126602172851562
		3.  Rewriting: 						0.8161067962646484


	Depth: 			3
	String length: 	3803
	Derivation Kernel/Function Runtimes (Milliseconds):
		1.  Successor counting: 			1.9121170043945312
		2.  Scan/prefix-sum of counts: 		0.050067901611328125
		3.  Rewriting: 						6.519079208374023


	Depth: 			4
	String length: 	30427
	Derivation Kernel/Function Runtimes (Milliseconds):
		1.  Successor counting: 			8.152961730957031
		2.  Scan/prefix-sum of counts: 		0.22101402282714844
		3.  Rewriting: 						52.47902870178223


	Depth: 			5
	String length: 	243419
	Derivation Kernel/Function Runtimes (Milliseconds):
		1.  Successor counting: 			69.3359375
		2.  Scan/prefix-sum of counts: 		2.103090286254883
		3.  Rewriting: 						439.5148754119873


	Depth: 			6
	String length: 	1947355
	Derivation Kernel/Function Runtimes (Milliseconds):
		1.  Successor counting: 			1083.446979522705
		2.  Scan/prefix-sum of counts: 		25.763988494873047
		3.  Rewriting: 						5544.972896575928


Interpretation Runtimes (Milliseconds):
		1.  Setting local matrices: 		72625.17619132996

Total Runtimes (Milliseconds):
	Derivation: 		11561.885118484497
	Interpretation: 	282022.2339630127