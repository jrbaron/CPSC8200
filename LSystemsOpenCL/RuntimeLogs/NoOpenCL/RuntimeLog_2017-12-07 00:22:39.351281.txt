L-System Generator Logfile
Created on 2017-12-07 00:22:39.351773
Author: Jessica Baron

L-System Attributes:
	Fractal Name: 				KochIsland
	Derivation Depth: 			9
	Line segment length: 		0.1
	Rotation angle (radians): 	1.570795
	Using OpenCL kernels? 		False
	Axiom String: 				A+A+A+A

Runtimes:
	Depth: 			0
	String length: 	7
	Derivation Kernel/Function Runtimes (Milliseconds):
		1.  Successor counting: 			0.016927719116210938
		2.  Scan/prefix-sum of counts: 		0.051021575927734375
		3.  Rewriting: 						0.024080276489257812


	Depth: 			1
	String length: 	59
	Derivation Kernel/Function Runtimes (Milliseconds):
		1.  Successor counting: 			0.029087066650390625
		2.  Scan/prefix-sum of counts: 		0.02193450927734375
		3.  Rewriting: 						0.10800361633300781


	Depth: 			2
	String length: 	475
	Derivation Kernel/Function Runtimes (Milliseconds):
		1.  Successor counting: 			0.15401840209960938
		2.  Scan/prefix-sum of counts: 		0.027179718017578125
		3.  Rewriting: 						0.9860992431640625


	Depth: 			3
	String length: 	3803
	Derivation Kernel/Function Runtimes (Milliseconds):
		1.  Successor counting: 			0.9579658508300781
		2.  Scan/prefix-sum of counts: 		0.041961669921875
		3.  Rewriting: 						6.326913833618164


	Depth: 			4
	String length: 	30427
	Derivation Kernel/Function Runtimes (Milliseconds):
		1.  Successor counting: 			7.597208023071289
		2.  Scan/prefix-sum of counts: 		0.15401840209960938
		3.  Rewriting: 						51.87511444091797


	Depth: 			5
	String length: 	243419
	Derivation Kernel/Function Runtimes (Milliseconds):
		1.  Successor counting: 			64.1179084777832
		2.  Scan/prefix-sum of counts: 		2.125978469848633
		3.  Rewriting: 						422.6999282836914


	Depth: 			6
	String length: 	1947355