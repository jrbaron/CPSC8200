L-System Generator Logfile
Created on 2017-12-07 00:36:45.891446
Author: Jessica Baron

L-System Attributes:
	Fractal Name: 				KochIsland
	Derivation Depth: 			6
	Line segment length: 		0.1
	Rotation angle (radians): 	1.570795
	Using OpenCL kernels? 		False
	Axiom String: 				A+A+A+A

Runtimes:
	Depth: 			0
	String length: 	7
	Derivation Kernel/Function Runtimes (Milliseconds):
		1.  Successor counting: 			0.022172927856445312
		2.  Scan/prefix-sum of counts: 		0.05412101745605469
		3.  Rewriting: 						0.027894973754882812


	Depth: 			1
	String length: 	59
	Derivation Kernel/Function Runtimes (Milliseconds):
		1.  Successor counting: 			0.027894973754882812
		2.  Scan/prefix-sum of counts: 		0.02288818359375
		3.  Rewriting: 						0.10895729064941406


	Depth: 			2
	String length: 	475
	Derivation Kernel/Function Runtimes (Milliseconds):
		1.  Successor counting: 			0.12087821960449219
		2.  Scan/prefix-sum of counts: 		0.025033950805664062
		3.  Rewriting: 						0.8170604705810547


	Depth: 			3
	String length: 	3803
	Derivation Kernel/Function Runtimes (Milliseconds):
		1.  Successor counting: 			1.1699199676513672
		2.  Scan/prefix-sum of counts: 		0.051975250244140625
		3.  Rewriting: 						6.851911544799805


	Depth: 			4
	String length: 	30427
	Derivation Kernel/Function Runtimes (Milliseconds):
		1.  Successor counting: 			7.695913314819336
		2.  Scan/prefix-sum of counts: 		0.20813941955566406
		3.  Rewriting: 						55.680036544799805


	Depth: 			5
	String length: 	243419
	Derivation Kernel/Function Runtimes (Milliseconds):
		1.  Successor counting: 			65.25588035583496
		2.  Scan/prefix-sum of counts: 		2.1381378173828125
		3.  Rewriting: 						436.98811531066895


Interpretation Runtimes (Milliseconds):
		1.  Setting local matrices: 		8403.440952301025

Total Runtimes (Milliseconds):
	Derivation: 		1457.62300491333
	Interpretation: 	25029.072999954224