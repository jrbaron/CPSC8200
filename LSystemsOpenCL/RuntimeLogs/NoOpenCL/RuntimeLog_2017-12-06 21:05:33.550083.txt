L-System Generator Logfile
Created on 2017-12-06 21:05:33.550449
Author: Jessica Baron

L-System Attributes:
	Fractal Name: 				KochIsland
	Derivation Depth: 			0
	Line segment length: 		0.1
	Rotation angle (radians): 	1.570795
	Using OpenCL kernels? 		False
	Axiom String: 				A+A+A+A

Runtimes:
Interpretation Runtimes (Milliseconds):
		1.  Setting local matrices: 		0.05412101745605469

Total Runtimes (Milliseconds):
	Derivation: 		0.19049644470214844
	Interpretation: 	0.16570091247558594