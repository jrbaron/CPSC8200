L-System Generator Logfile
Created on 2017-12-06 22:40:18.279475
Author: Jessica Baron

L-System Attributes:
	Fractal Name: 				KochIsland
	Derivation Depth: 			3
	Line segment length: 		0.1
	Rotation angle (radians): 	1.570795
	Using OpenCL kernels? 		False
	Axiom String: 				A+A+A+A

Runtimes:
	Depth: 			0
	String: 		A+A+A+A
	String length: 	7
	Derivation Kernel/Function Runtimes (Milliseconds):
		1.  Successor counting: 			0.012874603271484375
		2.  Scan/prefix-sum of counts: 		0.051021575927734375
		3.  Rewriting: 						0.0209808349609375


	Depth: 			1
	String: 		A+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A
	String length: 	59
	Derivation Kernel/Function Runtimes (Milliseconds):
		1.  Successor counting: 			0.04506111145019531
		2.  Scan/prefix-sum of counts: 		0.0209808349609375
		3.  Rewriting: 						0.10800361633300781


	Depth: 			2
	String: 		A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A-A+A-A-AA+A+A-AA+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A-A+A-A-AA+A+A-AA+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A-A+A-A-AA+A+A-AA+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A-A+A-A-AA+A+A-AA+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A
	String length: 	475
	Derivation Kernel/Function Runtimes (Milliseconds):
		1.  Successor counting: 			0.1220703125
		2.  Scan/prefix-sum of counts: 		0.026941299438476562
		3.  Rewriting: 						0.9329319000244141


Interpretation Runtimes (Milliseconds):
		1.  Setting local matrices: 		14.683961868286133

Total Runtimes (Milliseconds):
	Derivation: 		226.47500038146973
	Interpretation: 	47.03712463378906