L-System Generator Logfile
Created on 2017-12-07 00:36:29.673945
Author: Jessica Baron

L-System Attributes:
	Fractal Name: 				KochIsland
	Derivation Depth: 			1
	Line segment length: 		0.1
	Rotation angle (radians): 	1.570795
	Using OpenCL kernels? 		False
	Axiom String: 				A+A+A+A

Runtimes:
	Depth: 			0
	String length: 	7
	Derivation Kernel/Function Runtimes (Milliseconds):
		1.  Successor counting: 			0.02002716064453125
		2.  Scan/prefix-sum of counts: 		0.051021575927734375
		3.  Rewriting: 						0.022172927856445312


Interpretation Runtimes (Milliseconds):
		1.  Setting local matrices: 		0.24509429931640625

Total Runtimes (Milliseconds):
	Derivation: 		88.22011947631836
	Interpretation: 	0.8399486541748047