L-System Generator Logfile
Created on 2017-12-07 00:16:01.485404
Author: Jessica Baron

L-System Attributes:
	Fractal Name: 				KochIsland
	Derivation Depth: 			6
	Line segment length: 		0.1
	Rotation angle (radians): 	1.570795
	Using OpenCL kernels? 		False
	Axiom String: 				A+A+A+A

Runtimes:
	Depth: 			0
	String length: 	7
	Derivation Kernel/Function Runtimes (Milliseconds):
		1.  Successor counting: 			0.033855438232421875
		2.  Scan/prefix-sum of counts: 		0.08296966552734375
		3.  Rewriting: 						0.04792213439941406


	Depth: 			1
	String length: 	59
	Derivation Kernel/Function Runtimes (Milliseconds):
		1.  Successor counting: 			0.05984306335449219
		2.  Scan/prefix-sum of counts: 		0.03504753112792969
		3.  Rewriting: 						0.23102760314941406


	Depth: 			2
	String length: 	475
	Derivation Kernel/Function Runtimes (Milliseconds):
		1.  Successor counting: 			0.26607513427734375
		2.  Scan/prefix-sum of counts: 		0.03600120544433594
		3.  Rewriting: 						1.6629695892333984


	Depth: 			3
	String length: 	3803
	Derivation Kernel/Function Runtimes (Milliseconds):
		1.  Successor counting: 			2.177000045776367
		2.  Scan/prefix-sum of counts: 		0.07700920104980469
		3.  Rewriting: 						13.233184814453125


	Depth: 			4
	String length: 	30427
	Derivation Kernel/Function Runtimes (Milliseconds):
		1.  Successor counting: 			16.047954559326172
		2.  Scan/prefix-sum of counts: 		0.35500526428222656
		3.  Rewriting: 						108.04486274719238


	Depth: 			5
	String length: 	243419
	Derivation Kernel/Function Runtimes (Milliseconds):
		1.  Successor counting: 			130.53607940673828
		2.  Scan/prefix-sum of counts: 		3.8199424743652344
		3.  Rewriting: 						856.1429977416992


Interpretation Runtimes (Milliseconds):
		1.  Setting local matrices: 		7993.901014328003

Total Runtimes (Milliseconds):
	Derivation: 		2734.372138977051
	Interpretation: 	27016.727924346924