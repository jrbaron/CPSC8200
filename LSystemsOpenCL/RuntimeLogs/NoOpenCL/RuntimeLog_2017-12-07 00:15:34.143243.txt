L-System Generator Logfile
Created on 2017-12-07 00:15:34.143710
Author: Jessica Baron

L-System Attributes:
	Fractal Name: 				KochIsland
	Derivation Depth: 			0
	Line segment length: 		0.1
	Rotation angle (radians): 	1.570795
	Using OpenCL kernels? 		False
	Axiom String: 				A+A+A+A

Runtimes:
Interpretation Runtimes (Milliseconds):
		1.  Setting local matrices: 		0.07987022399902344

Total Runtimes (Milliseconds):
	Derivation: 		0.3650188446044922
	Interpretation: 	0.32782554626464844