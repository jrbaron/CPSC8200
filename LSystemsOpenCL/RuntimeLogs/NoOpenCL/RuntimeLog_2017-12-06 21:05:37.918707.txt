L-System Generator Logfile
Created on 2017-12-06 21:05:37.919063
Author: Jessica Baron

L-System Attributes:
	Fractal Name: 				KochIsland
	Derivation Depth: 			3
	Line segment length: 		0.1
	Rotation angle (radians): 	1.570795
	Using OpenCL kernels? 		False
	Axiom String: 				A+A+A+A

Runtimes:
	Depth: 			0
	String: 		A+A+A+A
	String length: 	7
	Derivation Kernel/Function Runtimes (Milliseconds):
		1.  Successor counting: 			0.0073909759521484375
		2.  Scan/prefix-sum of counts: 		0.021219253540039062
		3.  Rewriting: 						0.014781951904296875


	Depth: 			1
	String: 		A+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A
	String length: 	59
	Derivation Kernel/Function Runtimes (Milliseconds):
		1.  Successor counting: 			0.017881393432617188
		2.  Scan/prefix-sum of counts: 		0.012159347534179688
		3.  Rewriting: 						0.07653236389160156


	Depth: 			2
	String: 		A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A-A+A-A-AA+A+A-AA+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A-A+A-A-AA+A+A-AA+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A-A+A-A-AA+A+A-AA+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A-A+A-A-AA+A+A-AA+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A
	String length: 	475
	Derivation Kernel/Function Runtimes (Milliseconds):
		1.  Successor counting: 			0.09107589721679688
		2.  Scan/prefix-sum of counts: 		0.012159347534179688
		3.  Rewriting: 						0.5681514739990234


Interpretation Runtimes (Milliseconds):
		1.  Setting local matrices: 		12.04061508178711

Total Runtimes (Milliseconds):
	Derivation: 		111.85884475708008
	Interpretation: 	35.31241416931152