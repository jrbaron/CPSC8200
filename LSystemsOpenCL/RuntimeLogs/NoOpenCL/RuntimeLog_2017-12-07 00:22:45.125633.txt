L-System Generator Logfile
Created on 2017-12-07 00:22:45.126122
Author: Jessica Baron

L-System Attributes:
	Fractal Name: 				KochIsland
	Derivation Depth: 			10
	Line segment length: 		0.1
	Rotation angle (radians): 	1.570795
	Using OpenCL kernels? 		False
	Axiom String: 				A+A+A+A

Runtimes:
	Depth: 			0
	String length: 	7
	Derivation Kernel/Function Runtimes (Milliseconds):
		1.  Successor counting: 			0.018835067749023438
		2.  Scan/prefix-sum of counts: 		0.05793571472167969
		3.  Rewriting: 						0.025033950805664062


	Depth: 			1
	String length: 	59
	Derivation Kernel/Function Runtimes (Milliseconds):
		1.  Successor counting: 			0.026941299438476562
		2.  Scan/prefix-sum of counts: 		0.024080276489257812
		3.  Rewriting: 						0.14710426330566406


	Depth: 			2
	String length: 	475
	Derivation Kernel/Function Runtimes (Milliseconds):
		1.  Successor counting: 			0.1289844512939453
		2.  Scan/prefix-sum of counts: 		0.03409385681152344
		3.  Rewriting: 						1.0190010070800781


	Depth: 			3
	String length: 	3803
	Derivation Kernel/Function Runtimes (Milliseconds):
		1.  Successor counting: 			1.0101795196533203
		2.  Scan/prefix-sum of counts: 		0.051021575927734375
		3.  Rewriting: 						6.50787353515625


	Depth: 			4
	String length: 	30427
	Derivation Kernel/Function Runtimes (Milliseconds):
		1.  Successor counting: 			7.519960403442383
		2.  Scan/prefix-sum of counts: 		0.2040863037109375
		3.  Rewriting: 						51.0098934173584


	Depth: 			5
	String length: 	243419
	Derivation Kernel/Function Runtimes (Milliseconds):
		1.  Successor counting: 			64.48197364807129
		2.  Scan/prefix-sum of counts: 		1.9631385803222656
		3.  Rewriting: 						431.5810203552246


	Depth: 			6
	String length: 	1947355
