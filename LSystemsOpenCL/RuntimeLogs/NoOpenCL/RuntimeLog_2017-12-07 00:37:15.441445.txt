L-System Generator Logfile
Created on 2017-12-07 00:37:15.442324
Author: Jessica Baron

L-System Attributes:
	Fractal Name: 				KochIsland
	Derivation Depth: 			7
	Line segment length: 		0.1
	Rotation angle (radians): 	1.570795
	Using OpenCL kernels? 		False
	Axiom String: 				A+A+A+A

Runtimes:
	Depth: 			0
	String length: 	7
	Derivation Kernel/Function Runtimes (Milliseconds):
		1.  Successor counting: 			0.0171661376953125
		2.  Scan/prefix-sum of counts: 		0.051021575927734375
		3.  Rewriting: 						0.025033950805664062


	Depth: 			1
	String length: 	59
	Derivation Kernel/Function Runtimes (Milliseconds):
		1.  Successor counting: 			0.029802322387695312
		2.  Scan/prefix-sum of counts: 		0.022172927856445312
		3.  Rewriting: 						0.26106834411621094


	Depth: 			2
	String length: 	475
	Derivation Kernel/Function Runtimes (Milliseconds):
		1.  Successor counting: 			0.1430511474609375
		2.  Scan/prefix-sum of counts: 		0.028848648071289062
		3.  Rewriting: 						1.046895980834961


	Depth: 			3
	String length: 	3803
	Derivation Kernel/Function Runtimes (Milliseconds):
		1.  Successor counting: 			1.2810230255126953
		2.  Scan/prefix-sum of counts: 		0.04792213439941406
		3.  Rewriting: 						6.695985794067383


	Depth: 			4
	String length: 	30427
	Derivation Kernel/Function Runtimes (Milliseconds):
		1.  Successor counting: 			8.410930633544922
		2.  Scan/prefix-sum of counts: 		0.1690387725830078
		3.  Rewriting: 						55.841922760009766


	Depth: 			5
	String length: 	243419
	Derivation Kernel/Function Runtimes (Milliseconds):
		1.  Successor counting: 			67.36898422241211
		2.  Scan/prefix-sum of counts: 		2.084970474243164
		3.  Rewriting: 						447.4749565124512


	Depth: 			6
	String length: 	1947355
	Derivation Kernel/Function Runtimes (Milliseconds):
		1.  Successor counting: 			553.2350540161133
		2.  Scan/prefix-sum of counts: 		12.794971466064453
		3.  Rewriting: 						3640.209913253784


Interpretation Runtimes (Milliseconds):
		1.  Setting local matrices: 		63011.117935180664

Total Runtimes (Milliseconds):
	Derivation: 		8798.545837402344
	Interpretation: 	200417.9129600525