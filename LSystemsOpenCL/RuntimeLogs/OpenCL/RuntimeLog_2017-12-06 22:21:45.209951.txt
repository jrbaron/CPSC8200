L-System Generator Logfile
Created on 2017-12-06 22:21:45.210431
Author: Jessica Baron

L-System Attributes:
	Fractal Name: 				KochIsland
	Derivation Depth: 			0
	Line segment length: 		0.1
	Rotation angle (radians): 	1.570795
	Using OpenCL kernels? 		True
	Axiom String: 				A+A+A+A

Runtimes:
Interpretation Runtimes (Milliseconds):
		1.  Setting local matrices: 		1.300811767578125

Total Runtimes (Milliseconds):
	Derivation: 		0.6048679351806641
	Interpretation: 	6.073951721191406