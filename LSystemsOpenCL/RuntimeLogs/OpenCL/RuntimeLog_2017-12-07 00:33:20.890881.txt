L-System Generator Logfile
Created on 2017-12-07 00:33:20.891515
Author: Jessica Baron

L-System Attributes:
	Fractal Name: 				KochIsland
	Derivation Depth: 			0
	Line segment length: 		0.1
	Rotation angle (radians): 	1.570795
	Using OpenCL kernels? 		True
	Axiom String: 				A+A+A+A

Runtimes:
Interpretation Runtimes (Milliseconds):
		1.  Setting local matrices: 		1.4119148254394531

Total Runtimes (Milliseconds):
	Derivation: 		0.49614906311035156
	Interpretation: 	7.374048233032227