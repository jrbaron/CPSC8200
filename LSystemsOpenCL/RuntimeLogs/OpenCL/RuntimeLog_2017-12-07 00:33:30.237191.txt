L-System Generator Logfile
Created on 2017-12-07 00:33:30.237673
Author: Jessica Baron

L-System Attributes:
	Fractal Name: 				KochIsland
	Derivation Depth: 			4
	Line segment length: 		0.1
	Rotation angle (radians): 	1.570795
	Using OpenCL kernels? 		True
	Axiom String: 				A+A+A+A

Runtimes:
	Depth: 			0
	String length: 	7
	Derivation Kernel/Function Runtimes (Milliseconds):
		1.  Successor counting: 			1.4920234680175781
		2.  Scan/prefix-sum of counts: 		4.12297248840332
		3.  Rewriting: 						0.47087669372558594


	Depth: 			1
	String length: 	59
	Derivation Kernel/Function Runtimes (Milliseconds):
		1.  Successor counting: 			0.35691261291503906
		2.  Scan/prefix-sum of counts: 		1.0251998901367188
		3.  Rewriting: 						0.5800724029541016


	Depth: 			2
	String length: 	475
	Derivation Kernel/Function Runtimes (Milliseconds):
		1.  Successor counting: 			0.40602684020996094
		2.  Scan/prefix-sum of counts: 		1.1219978332519531
		3.  Rewriting: 						0.5800724029541016


	Depth: 			3
	String length: 	3803
	Derivation Kernel/Function Runtimes (Milliseconds):
		1.  Successor counting: 			0.5810260772705078
		2.  Scan/prefix-sum of counts: 		1.0790824890136719
		3.  Rewriting: 						0.9958744049072266


Interpretation Runtimes (Milliseconds):
		1.  Setting local matrices: 		1.9698143005371094

Total Runtimes (Milliseconds):
	Derivation: 		344.6969985961914
	Interpretation: 	273.2720375061035