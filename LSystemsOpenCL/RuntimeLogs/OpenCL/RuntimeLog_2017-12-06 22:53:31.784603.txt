L-System Generator Logfile
Created on 2017-12-06 22:53:31.785098
Author: Jessica Baron

L-System Attributes:
	Fractal Name: 				KochIsland
	Derivation Depth: 			3
	Line segment length: 		0.1
	Rotation angle (radians): 	1.570795
	Using OpenCL kernels? 		True
	Axiom String: 				A+A+A+A

Runtimes:
	Depth: 			0
	String: 		A+A+A+A
	String length: 	7
	Derivation Kernel/Function Runtimes (Milliseconds):
		1.  Successor counting: 			1.5249252319335938
		2.  Scan/prefix-sum of counts: 		1.2209415435791016
		3.  Rewriting: 						0.49686431884765625


	Depth: 			1
	String: 		A+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A
	String length: 	59
	Derivation Kernel/Function Runtimes (Milliseconds):
		1.  Successor counting: 			0.45609474182128906
		2.  Scan/prefix-sum of counts: 		1.2030601501464844
		3.  Rewriting: 						0.4839897155761719


	Depth: 			2
	String: 		A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A-A+A-A-AA+A+A-AA+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A-A+A-A-AA+A+A-AA+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A-A+A-A-AA+A+A-AA+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A-A+A-A-AA+A+A-AA+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A
	String length: 	475
	Derivation Kernel/Function Runtimes (Milliseconds):
		1.  Successor counting: 			0.43702125549316406
		2.  Scan/prefix-sum of counts: 		1.1491775512695312
		3.  Rewriting: 						0.5609989166259766


Interpretation Runtimes (Milliseconds):
		1.  Setting local matrices: 		0.8890628814697266

Total Runtimes (Milliseconds):
	Derivation: 		245.589017868042
	Interpretation: 	39.63804244995117