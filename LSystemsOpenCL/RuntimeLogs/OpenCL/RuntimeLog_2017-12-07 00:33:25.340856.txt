L-System Generator Logfile
Created on 2017-12-07 00:33:25.341455
Author: Jessica Baron

L-System Attributes:
	Fractal Name: 				KochIsland
	Derivation Depth: 			2
	Line segment length: 		0.1
	Rotation angle (radians): 	1.570795
	Using OpenCL kernels? 		True
	Axiom String: 				A+A+A+A

Runtimes:
	Depth: 			0
	String length: 	7
	Derivation Kernel/Function Runtimes (Milliseconds):
		1.  Successor counting: 			1.5671253204345703
		2.  Scan/prefix-sum of counts: 		1.0900497436523438
		3.  Rewriting: 						0.5459785461425781


	Depth: 			1
	String length: 	59
	Derivation Kernel/Function Runtimes (Milliseconds):
		1.  Successor counting: 			0.36716461181640625
		2.  Scan/prefix-sum of counts: 		1.1920928955078125
		3.  Rewriting: 						0.41413307189941406


Interpretation Runtimes (Milliseconds):
		1.  Setting local matrices: 		3.8499832153320312

Total Runtimes (Milliseconds):
	Derivation: 		172.26910591125488
	Interpretation: 	11.668920516967773