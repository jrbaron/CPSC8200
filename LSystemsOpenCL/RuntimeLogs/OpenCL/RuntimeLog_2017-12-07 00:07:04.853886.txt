L-System Generator Logfile
Created on 2017-12-07 00:07:04.854383
Author: Jessica Baron

L-System Attributes:
	Fractal Name: 				KochIsland
	Derivation Depth: 			1
	Line segment length: 		0.1
	Rotation angle (radians): 	1.570795
	Using OpenCL kernels? 		True
	Axiom String: 				A+A+A+A

Runtimes:
	Depth: 			0
	String length: 	7
	Derivation Kernel/Function Runtimes (Milliseconds):
		1.  Successor counting: 			1.416921615600586
		2.  Scan/prefix-sum of counts: 		1.0671615600585938
		3.  Rewriting: 						0.48995018005371094


Interpretation Runtimes (Milliseconds):
		1.  Setting local matrices: 		0.5078315734863281

Total Runtimes (Milliseconds):
	Derivation: 		88.81497383117676
	Interpretation: 	6.039857864379883