L-System Generator Logfile
Created on 2017-12-07 00:33:59.167725
Author: Jessica Baron

L-System Attributes:
	Fractal Name: 				KochIsland
	Derivation Depth: 			7
	Line segment length: 		0.1
	Rotation angle (radians): 	1.570795
	Using OpenCL kernels? 		True
	Axiom String: 				A+A+A+A

Runtimes:
	Depth: 			0
	String length: 	7
	Derivation Kernel/Function Runtimes (Milliseconds):
		1.  Successor counting: 			5.3730010986328125
		2.  Scan/prefix-sum of counts: 		3.7419795989990234
		3.  Rewriting: 						0.5738735198974609


	Depth: 			1
	String length: 	59
	Derivation Kernel/Function Runtimes (Milliseconds):
		1.  Successor counting: 			0.47397613525390625
		2.  Scan/prefix-sum of counts: 		0.9889602661132812
		3.  Rewriting: 						0.4420280456542969


	Depth: 			2
	String length: 	475
	Derivation Kernel/Function Runtimes (Milliseconds):
		1.  Successor counting: 			0.576019287109375
		2.  Scan/prefix-sum of counts: 		1.3179779052734375
		3.  Rewriting: 						0.5970001220703125


	Depth: 			3
	String length: 	3803
	Derivation Kernel/Function Runtimes (Milliseconds):
		1.  Successor counting: 			0.5679130554199219
		2.  Scan/prefix-sum of counts: 		1.0249614715576172
		3.  Rewriting: 						0.8161067962646484


	Depth: 			4
	String length: 	30427
	Derivation Kernel/Function Runtimes (Milliseconds):
		1.  Successor counting: 			0.949859619140625
		2.  Scan/prefix-sum of counts: 		1.0819435119628906
		3.  Rewriting: 						3.320932388305664


	Depth: 			5
	String length: 	243419
	Derivation Kernel/Function Runtimes (Milliseconds):
		1.  Successor counting: 			1.405954360961914
		2.  Scan/prefix-sum of counts: 		1.4851093292236328
		3.  Rewriting: 						5.383968353271484


	Depth: 			6
	String length: 	1947355
	Derivation Kernel/Function Runtimes (Milliseconds):
		1.  Successor counting: 			3.937959671020508
		2.  Scan/prefix-sum of counts: 		4.54401969909668
		3.  Rewriting: 						18.451929092407227


Interpretation Runtimes (Milliseconds):
		1.  Setting local matrices: 		485.54301261901855

Total Runtimes (Milliseconds):
	Derivation: 		4962.996006011963
	Interpretation: 	134539.17908668518