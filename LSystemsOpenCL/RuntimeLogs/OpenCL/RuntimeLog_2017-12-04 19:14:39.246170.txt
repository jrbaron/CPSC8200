L-System Generator Logfile
Created on 2017-12-04 19:14:39.246519
Author: Jessica Baron

L-System Attributes:
	Fractal Name: 				KochIsland
	Derivation Depth: 			1
	Line segment length: 		0.1
	Rotation angle (radians): 	1.570795
	Using OpenCL kernels? 		True
	Axiom String: 				A+A+A+A

Runtimes:
	Depth: 			0
	String: 		A+A+A+A
	String length: 	7
	Derivation Kernel/Function Runtimes (Milliseconds):
		1.  Successor counting: 			0.5345344543457031
		2.  Scan/prefix-sum of counts: 		0.1666545867919922
		3.  Rewriting: 						0.1666545867919922


Interpretation Runtimes (Milliseconds):
		1.  Setting local matrices: 		0.232696533203125

Total Runtimes (Milliseconds):
	Derivation: 		40.46988487243652
	Interpretation: 	0.9129047393798828