L-System Generator Logfile
Created on 2017-12-07 00:09:31.750852
Author: Jessica Baron

L-System Attributes:
	Fractal Name: 				KochIsland
	Derivation Depth: 			2
	Line segment length: 		0.1
	Rotation angle (radians): 	1.570795
	Using OpenCL kernels? 		True
	Axiom String: 				A+A+A+A

Runtimes:
	Depth: 			0
	String length: 	7
	Derivation Kernel/Function Runtimes (Milliseconds):
		1.  Successor counting: 			1.5079975128173828
		2.  Scan/prefix-sum of counts: 		1.0800361633300781
		3.  Rewriting: 						0.5180835723876953


	Depth: 			1
	String length: 	59
	Derivation Kernel/Function Runtimes (Milliseconds):
		1.  Successor counting: 			0.39505958557128906
		2.  Scan/prefix-sum of counts: 		0.9829998016357422
		3.  Rewriting: 						0.5679130554199219


Interpretation Runtimes (Milliseconds):
		1.  Setting local matrices: 		0.7429122924804688

Total Runtimes (Milliseconds):
	Derivation: 		168.39289665222168
	Interpretation: 	11.469125747680664