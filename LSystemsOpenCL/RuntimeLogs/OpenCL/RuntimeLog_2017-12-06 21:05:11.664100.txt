L-System Generator Logfile
Created on 2017-12-06 21:05:11.664472
Author: Jessica Baron

L-System Attributes:
	Fractal Name: 				KochIsland
	Derivation Depth: 			3
	Line segment length: 		0.1
	Rotation angle (radians): 	1.570795
	Using OpenCL kernels? 		True
	Axiom String: 				A+A+A+A

Runtimes:
	Depth: 			0
	String: 		A+A+A+A
	String length: 	7
	Derivation Kernel/Function Runtimes (Milliseconds):
		1.  Successor counting: 			0.5388259887695312
		2.  Scan/prefix-sum of counts: 		0.1633167266845703
		3.  Rewriting: 						0.06318092346191406


	Depth: 			1
	String: 		A+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A
	String length: 	59
	Derivation Kernel/Function Runtimes (Milliseconds):
		1.  Successor counting: 			0.1461505889892578
		2.  Scan/prefix-sum of counts: 		0.1728534698486328
		3.  Rewriting: 						0.054836273193359375


	Depth: 			2
	String: 		A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A-A+A-A-AA+A+A-AA+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A-A+A-A-AA+A+A-AA+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A-A+A-A-AA+A+A-AA+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A-A+A-A-AA+A+A-AA+A-A-AA+A+A-A+A+A-A-AA+A+A-A+A+A-A-AA+A+A-A-A+A-A-AA+A+A-A
	String length: 	475
	Derivation Kernel/Function Runtimes (Milliseconds):
		1.  Successor counting: 			0.24080276489257812
		2.  Scan/prefix-sum of counts: 		0.1537799835205078
		3.  Rewriting: 						0.15234947204589844


Interpretation Runtimes (Milliseconds):
		1.  Setting local matrices: 		0.31304359436035156

Total Runtimes (Milliseconds):
	Derivation: 		114.68124389648438
	Interpretation: 	22.092580795288086