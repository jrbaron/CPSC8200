L-System Generator Logfile
Created on 2017-12-06 22:35:14.194408
Author: Jessica Baron

L-System Attributes:
	Fractal Name: 				KochIsland
	Derivation Depth: 			0
	Line segment length: 		0.1
	Rotation angle (radians): 	1.570795
	Using OpenCL kernels? 		True
	Axiom String: 				A+A+A+A

Runtimes:
Interpretation Runtimes (Milliseconds):
		1.  Setting local matrices: 		1.4510154724121094

Total Runtimes (Milliseconds):
	Derivation: 		0.34618377685546875
	Interpretation: 	6.104946136474609