#Derive/expand an axiom string of an L-system based on a set of productions defined in an OpenCL 2D image.

import numpy as np
import pyopencl as cl
from pyopencl.scan import GenericScanKernel
import pyopencl.array
from time import time
import math

#---------------------------------------------

class LSystemGenerator():

    def __init__(self, name, depth, useOpenCL, fwdStep, rotRad):
        self.printCLInfo()

        self.name = name
        self.depth = depth
        self.useOpenCL = useOpenCL
        self.fwdStep = fwdStep
        self.rotRad = rotRad

        #self.context = cl.create_some_context()
    #More manual for Mac OS vs. Ubuntu since multiple devices found.
        platforms = cl.get_platforms()
        devices = platforms[0].get_devices(device_type=cl.device_type.GPU)
        self.context = cl.Context([devices[-1]])    #The second (and last) GPU device on Mac is the graphics card vs. the on-board graphics.
        self.context.get_info(cl.context_info.DEVICES)

        self.cmdQueue = cl.CommandQueue(self.context)

        #print("Using CL context: ", self.context)

        self.kernelSource = open('model/kernels/derive.cl').read()
        self.program = cl.Program(self.context, self.kernelSource)
        self.program = self.program.build()

        self.productions = []
        self.prods_img = None
        self.headerSize = 1   #Will contain count of successors. #TODO: Expand header (to size 2) to also support parameters.
        self.numProds = 0
        self.maxProdLen = 0   #Longest production is maxProdLen characters.
        #maxProdLen = 16   #TODO: Do I have to round up to a power of 2 (for the OpenCL Image type)?  Or does PyOpenCL handle that?

  #---------------------------
    #https://github.com/oysstu/pyopencl-in-action/blob/master/opencl-print-info.py
    def printCLInfo(self):
        '''
        Prints relevant information regarding the capabilities of the current OpenCL runtime and devices
        Note that pyopencl has a script that prints all properties in its examples folder
        '''

        import pyopencl as cl

        print('PyOpenCL version: ' + cl.VERSION_TEXT)
        print('OpenCL header version: ' + '.'.join(map(str, cl.get_cl_header_version())) + '\n')

        # Get installed platforms (SDKs)
        print('- Installed platforms (SDKs) and available devices:')
        platforms = cl.get_platforms()

        for plat in platforms:
            indent = ''

            # Get and print platform info
            print(indent + '{} ({})'.format(plat.name, plat.vendor))
            indent = '\t'
            print(indent + 'Version: ' + plat.version)
            print(indent + 'Profile: ' + plat.profile)
            print(indent + 'Extensions: ' + str(plat.extensions.strip().split(' ')))

            # Get and print device info
            devices = plat.get_devices(cl.device_type.ALL)

            print(indent + 'Available devices: ')
            if not devices:
                print(indent + '\tNone')

            for dev in devices:
                indent = '\t\t'
                print(indent + '{} ({})'.format(dev.name, dev.vendor))

                indent = '\t\t\t'
                flags = [('Version', dev.version),
                         ('Type', cl.device_type.to_string(dev.type)),
                         ('Extensions', str(dev.extensions.strip().split(' '))),
                         ('Memory (global)', str(dev.global_mem_size)),
                         ('Memory (local)', str(dev.local_mem_size)),
                         ('Address bits', str(dev.address_bits)),
                         ('Max work item dims', str(dev.max_work_item_dimensions)),
                         ('Max work group size', str(dev.max_work_group_size)),
                         ('Max compute units', str(dev.max_compute_units)),
                         ('Driver version', dev.driver_version),
                         ('Image support', str(bool(dev.image_support))),
                         ('Little endian', str(bool(dev.endian_little))),
                         ('Device available', str(bool(dev.available))),
                         ('Compiler available', str(bool(dev.compiler_available)))]

                [print(indent + '{0:<25}{1:<10}'.format(name + ':', flag)) for name, flag in flags]

                # Device version string has the following syntax, extract the number like this
                # OpenCL<space><major_version.minor_version><space><vendor-specific information>
                version_number = float(dev.version.split(' ')[1])

        print('')

  #---------------------------
    #Called outside of this class.
    def expandString(self, string, logfile, expansions=None, useOpenCL=None):
        self.prod_img = self.makeProductionsImage()

        #These two can be initially set in the LSystemGenerator
        #  constructor but can also be set here.
        if expansions is not None:
            self.depth = expansions
        if useOpenCL is not None:
            self.useOpenCL = useOpenCL

        logfile.write("\nRuntimes:")
        for i in range(self.depth):
            logfile.write("\n\tDepth: \t\t\t" + str(i))
            logfile.write("\n\tString: \t\t" + string)
            logfile.write("\n\tString length: \t" + str(len(string)))
            string = self.deriveString(string, self.prod_img, logfile)
            logfile.write("\n\n")

        return string

  #---------------------------
    def makeProdList(self, prodString):
        succCount = 0
        prod = list(prodString)
        symbol = chr(self.numProds + ord('A'))

        self.numProds += 1
        sz = len(prod)
        if (sz+self.headerSize) > self.maxProdLen:
            self.maxProdLen = sz+self.headerSize     #Full production char length includes the header.

        for i in range(sz):
            if prod[i] != '(' and prod[i] != ')':  #Parentheses hold module params.
                succCount += 1                     #Everything else is a successor module.
            prod[i] = ord(prod[i])

        prod.insert(0, succCount)

        return prod

  #---------------------------
    #Called outside of this class.
    def addProduction(self, prod):
        prodList = self.makeProdList(prod)

        #for itm in prodList:
        #    self.productions.append(itm)
        self.productions.append(prodList)

    #'!' is added for fillers after all productions have been added and max length known.

  #---------------------------
    def makeProductionsImage(self):

        unpackedProds = []
        for prod in self.productions:
            sz = len(prod)
            for i in range(self.maxProdLen-sz):
                prod.append(33)  #fill in with '!'
        for prod in self.productions:
            for itm in prod:
                unpackedProds.append(itm)

        productions = np.array( self.productions, dtype=np.uint8)

        #print("HEADER SIZE: ", self.headerSize)
        #print("MAX PROD LEN (incl header): ", self.maxProdLen)
        #print("NUM PRODS: ", self.numProds)
        #print("Prods NumPy Array:", productions)

      #Channel order: One channel for grayscale images or matrix elements.
      #Channel type: For chars (describing rules) and 0..255 ints (the successor/param counts)
        imgFormat = cl.ImageFormat( cl.channel_order.R, cl.channel_type.UNSIGNED_INT8)
        prods_img = cl.Image(self.context,
             flags=cl.mem_flags.READ_ONLY | cl.mem_flags.COPY_HOST_PTR,  #Latter flag needed when setting hostbuf param.
             format=imgFormat,
             shape=(self.maxProdLen, self.numProds),
             hostbuf=productions
             )

        return prods_img

  #---------------------------
    #prods_img needs to be a local variable through a parameter?
    # Memory issue if trying to directly use the class variable self.prods_img.
    def deriveString(self, string, prods_img, logfile):
        countsTime = 0
        countsScanTime = 0
        rewriteTime = 0

  #1.  Get initial successor/parameter counts from each char of the string to rewrite.
    #print("host_axiom string: ", host_axiom)
        host_axiom = list(string)
        for i in range(len(host_axiom)):
            host_axiom[i] = ord(host_axiom[i])
        host_axiom = np.array(host_axiom, np.uint8)
        numLetters = len(host_axiom)

        host_letterCounts = np.empty(numLetters).astype(np.int32)   #This is later used in a prefix sum, which needs a data type divisible by 4.
    #print('host_axiom array: ', host_axiom)
    #print('\tNum letters is: ', numLetters)

        dev_axiom = cl.Buffer(self.context, cl.mem_flags.READ_ONLY | cl.mem_flags.COPY_HOST_PTR, hostbuf=host_axiom)
        dev_letterCounts = cl.Buffer(self.context, cl.mem_flags.WRITE_ONLY, host_letterCounts.nbytes)


  #Get the kernel function from the program.
        countsToDerive = self.program.countsToDerive

  #Inform wrappers of size per param. None for "non-scalars."
        countsToDerive.set_scalar_arg_dtypes( [None, np.uint32, np.uint32, None, None, np.uint32] )  #uint32 for CL's uint

#__cal__(queue, global_size, local_size, *args, .. (a few defaulted params))
#global_size affects the IDs from get_global_id().
        if self.useOpenCL:
            runtime = time()
            countsToDerive(self.cmdQueue, host_axiom.shape, None,
               prods_img, self.numProds, self.maxProdLen,
               dev_axiom, dev_letterCounts, numLetters)
            self.cmdQueue.finish()
            countsTime = time()-runtime
            cl.enqueue_copy(self.cmdQueue, host_letterCounts, dev_letterCounts)
        else:
            runtime = time()
            counts = self.SEQ_countsToDerive(string)
            host_letterCounts = np.array(counts, dtype=np.int32)
            countsTime = time()-runtime

        #print("Letter counts:  ", host_letterCounts)
        rewriteSize = host_letterCounts[-1]         #Save the length of last item to expand.

  #---------------
  #2.  Prefix sum of the counts
#  How is the NumPy function cumsum() implemented? What's the efficiency? Is it in parallel?
#    I think Kloeckner (PyOpenCL dude) points it out only for getting an idea of the scan op,
#    not that this implementation is parallel.
#  What about the PyOpenCL GenericScanKernel?  That's at least on the CL device and hopefully
#    more optimized than me manually making a new scan kernel.

        #The kernel needs a data type divisible by 4.
        rewriteOffsets = cl.array.Array(self.cmdQueue, shape=host_letterCounts.shape, dtype=np.int32)
        rewriteOffsets.set(host_letterCounts)

        scanKernel = GenericScanKernel(
            self.context, np.int32,
            arguments="__global int *countsArr",
            input_expr="countsArr[i-1]",
            scan_expr="a+b", neutral="0",
            output_statement="countsArr[i] = item;")

        if self.useOpenCL:
            #print("Offsets before scan: ", rewriteOffsets)
            runtime = time()
            scanKernel(rewriteOffsets, queue=self.cmdQueue)
            self.cmdQueue.finish()
            countsScanTime = time()-runtime
            host_offsets = rewriteOffsets.get()    #TODO: Sometimes this makes no sense and causes a memory error at host_newString?
            #print("Offsets after scan", host_offsets)
            #print("\ttype is", type(host_offsets))
        else:
            #print("Offsets before scan: ", host_letterCounts)
            runtime = time()
            host_offsets = np.cumsum(host_letterCounts)
            countsScanTime = time()-runtime
            zeroFirst = np.empty(numLetters).astype(np.int32)
            zeroFirst[0] = 0
            zeroFirst[1:] = host_offsets[:-1]
            host_offsets = zeroFirst
            #print("Offsets after scan", host_offsets)
            #print("\ttype is", type(host_offsets))

    #print("Prefix sum of counts (offsets used in rewriting:", rewriteOffsets)

  #---------------
  #3.  Rewrite the string using the offsets.
        rewriteSize += host_offsets[-1]   #Size of last symbol to expand plus where it starts in the output string.
        #print("rewriteSize: ", rewriteSize)
        host_newString = np.empty(rewriteSize).astype(np.uint8)
    #print(host_offsets, type(host_offsets))
    #print("Size of new string: ", rewriteSize, type(rewriteSize))
    #print("New string: ", host_newString)

        dev_offsets =  cl.Buffer(self.context, cl.mem_flags.READ_ONLY | cl.mem_flags.COPY_HOST_PTR, hostbuf=host_offsets)
        dev_axiom = cl.Buffer(self.context, cl.mem_flags.READ_ONLY | cl.mem_flags.COPY_HOST_PTR, hostbuf=host_axiom)
        dev_newString = cl.Buffer(self.context, cl.mem_flags.WRITE_ONLY, host_newString.nbytes)

        rewriteKernel = self.program.rewriteString
        rewriteKernel.set_scalar_arg_dtypes( [None, None, None, None, np.uint32, np.uint32] )

        if self.useOpenCL:
            runtime = time()
            rewriteKernel(self.cmdQueue, host_axiom.shape, None,
                  prods_img, dev_offsets, dev_axiom,
                  dev_newString, numLetters, self.headerSize)
            self.cmdQueue.finish()
            rewriteTime = time()-runtime
            cl.enqueue_copy(self.cmdQueue, host_newString, dev_newString)
        else:
            runtime = time()
            host_newString = self.SEQ_rewriteString(host_offsets, string)
            rewriteTime = time()-runtime


    #print("New string: ", host_newString)
        charString = []
        for itm in host_newString:
            charString.append(chr(itm))
        newStr = ''.join(charString)

        logfile.write("\n\tDerivation Kernel/Function Runtimes (Milliseconds):")
        logfile.write("\n\t\t1.  Successor counting: \t\t\t" + str(countsTime*1000))
        logfile.write("\n\t\t2.  Scan/prefix-sum of counts: \t\t" + str(countsScanTime*1000))
        logfile.write("\n\t\t3.  Rewriting: \t\t\t\t\t\t" + str(rewriteTime*1000))

        return newStr

  #-------------------------------------
    def makeVBO(self, logfile, string, startPos, thetaRad=None, fwdStep=None):

      #If given, override the initially set values.
        if thetaRad is not None:
            self.rotRad = thetaRad
        if fwdStep is not None:
            self.fwdStep = fwdStep

        host_string = list(string)
        for i in range(len(host_string)):
            host_string[i] = ord(host_string[i])
        host_string = np.array(host_string, np.uint8)
        numLetters = len(host_string)

      #1.  Set local transformation matrices per letter of the symbol.
        if self.useOpenCL:
            host_matrices = np.zeros(numLetters*4*4, dtype=np.float32)  #4x4 transformation matrices.
            host_drawCounts = np.empty(numLetters).astype(np.int32)

            dev_string =  cl.Buffer(self.context, cl.mem_flags.READ_ONLY | cl.mem_flags.COPY_HOST_PTR, hostbuf=host_string)
            dev_matrices = cl.Buffer(self.context, cl.mem_flags.WRITE_ONLY, host_matrices.nbytes)
            dev_drawCounts = cl.Buffer(self.context, cl.mem_flags.WRITE_ONLY, host_drawCounts.nbytes)

            localMatsKernel = self.program.setLocalMats
            localMatsKernel.set_scalar_arg_dtypes( [None, None, None, np.uint32, np.float32, np.float32] )
            runtime = time()
            localMatsKernel(self.cmdQueue, host_string.shape, None,
                        dev_string, dev_matrices, dev_drawCounts,
                        numLetters, self.rotRad, self.fwdStep)
            self.cmdQueue.finish()
            runtime = time()-runtime
            cl.enqueue_copy(self.cmdQueue, host_matrices, dev_matrices)
            cl.enqueue_copy(self.cmdQueue, host_drawCounts, dev_drawCounts)


        else:
            runtime = time()
            host_matrices, host_drawCounts = self.SEQ_setLocalMats(string, self.fwdStep, self.rotRad)
            runtime = time()-runtime

        logfile.write("\nInterpretation Runtimes (Milliseconds):")
        logfile.write("\n\t\t1.  Setting local matrices: \t\t" + str(runtime*1000))


    #2.  Scan local trans. matrices to store multiplied global matrix transform for each step of drawing.
        localTransMats = []
        for i in range(0, numLetters*16, 16):

            arr = []
            for j in range(0, 16, 4):
                k = i+j
                arr.append(host_matrices[k:k+4])
            #mat = np.matrix(arr)
            localTransMats.append(arr)

        localTransMats = np.array(localTransMats)
        globalTransMats = self.scanMatMult(localTransMats)

     #   print("~~Local~~")
     #   self.printTransMats(localTransMats, string)
     #   print("~~Global~~")
     #   self.printTransMats(globalTransMats, string)

    #3.  Get drawing positions for each drawing object by the global transforms.
        if self.useOpenCL:
            positions = self.SEQ_setPositions(string, host_drawCounts, globalTransMats, startPos)
        else:
            positions = self.SEQ_setPositions(string, host_drawCounts, globalTransMats, startPos)


        return positions

  #-------------------------------------
    def scanMatMult(self, localTransMats):
        globalMats = []
        for m in range(len(localTransMats)):
            mat = localTransMats[m]

            if m == 0:
                globalMats.append(mat)
                lastMat = mat
            else:
                #newMat = mat * lastMat   #Reverse order for column-major
                newMat = np.dot(lastMat, mat)

                #print("\n\nMULTIPLYING")
                #print(lastMat)
                #print(" * \n", mat)
                #print(" = \n", newMat)
                globalMats.append(newMat)
                lastMat = newMat

            #print("m = ", m)
            #print("\tmat", mat)

        return np.array(globalMats)

  #-------------------------------------
    def printTransMats(self, mats, string):
         print("Trans mats: ")
         for m in range(len(mats)):
             mat = mats[m]
             print("mat for char: ", string[m])
             for row in mat:
                 print("\t", row)

#-------------------------------------
#SEQUENTIAL (non-OpenCL) first part of L-system derivation/rewriting stage.
#Gather counts to aid in memory allocation for rewriting.
    def SEQ_countsToDerive(self, string):
        numLetters = len(string)
        letterCounts = [0]*numLetters
        for id in range(numLetters):
            ch = string[id]
            o = ord(ch)

       #If the character is a capital letter [A..Z]
            if 65 <= o and o <= 90:
                prod = o-65
                letter = 0

                cnt = self.productions[prod][letter]
                letterCounts[id] = cnt

            else:   #The char will be rewritten with itself.
                letterCounts[id] = 1

        return letterCounts


#--------------------------------
#SEQUENTIAL (non-OpenCL) second part of L-system derivation/rewriting stage.
#Actually rewrite each symbol of the string based on
#  the counts of the previous step and production rules.
    def SEQ_rewriteString(self, offsets, inputStr):
        numLetters = len(inputStr)
        outputStr = []

        for id in range(numLetters):
            ch = inputStr[id]
            o = ord(ch)
            offset_ind = offsets[id]

            if 65 <= o and o <= 90:
                prod = o-65
                letter = 0
                lenElem = self.productions[prod][letter]

              #Copy over the successor.
                for i in range(lenElem):
                    letter = i+self.headerSize    #+1 to exclude the header
                    elem = self.productions[prod][letter]
                    #outputStr[offset_ind+i] = elem
                    outputStr.append(elem)

            else:
                #outputStr[offset_ind] = o
                outputStr.append(o)

        return outputStr


#-------------------------------------
    def SEQ_setLocalMats(self, inputStr, fwdStep, thetaRad):

        numLetters = len(inputStr)
        mats = np.zeros( (numLetters*4, 4), dtype=np.float32 )
        drawCounts = np.empty(numLetters, np.int32)

        for id in range(numLetters):
            matID = id * 4
            ch = inputStr[id]

            if ch == 'A':
                mats[matID] = [1.0, 0.0, 0.0, 0.0]
                mats[matID+1] = [0.0, 1.0, 0.0, fwdStep]
                mats[matID+2] = [0.0, 0.0, 1.0, 0.0]
                mats[matID+3] = [0.0, 0.0, 0.0, 1.0]
                drawCounts[id] = 1

            elif ch == '+':
                mats[matID] = [math.cos(thetaRad), -math.sin(thetaRad), 0.0, 0.0]
                mats[matID+1] = [math.sin(thetaRad), math.cos(thetaRad), 0.0, 0.0]
                mats[matID+2] = [0.0, 0.0, 1.0, 0.0]
                mats[matID+3] = [0.0, 0.0, 0.0, 1.0]
                drawCounts[id] = 0

            elif ch == '-':
                mats[matID] = [math.cos(-thetaRad), -math.sin(-thetaRad), 0.0, 0.0]
                mats[matID+1] = [math.sin(-thetaRad), math.cos(-thetaRad), 0.0, 0.0]
                mats[matID+2] = [0.0, 0.0, 1.0, 0.0]
                mats[matID+3] = [0.0, 0.0, 0.0, 1.0]
                drawCounts[id] = 0

            else:
                mats[matID] = [1.0, 0.0, 0.0, 0.0]
                mats[matID+1] = [0.0, 1.0, 0.0, 0.0]
                mats[matID+2] = [0.0, 0.0, 1.0, 0.0]
                mats[matID+3] = [0.0, 0.0, 0.0, 1.0]
                drawCounts[id] = 0

        return mats.flatten(), drawCounts

    #-------------------------------------
    def SEQ_setPositions(self, string, drawCounts, globalTransMats, startPos):
        numLetters = len(string)

        scanDrawCounts = np.cumsum(drawCounts)
        positions = np.empty( (scanDrawCounts[-1]+1)*3, dtype=np.float32 )
        #print("Draw counts: ", host_drawCounts)
        #print("Scanned draw counts: ", scanDrawCounts)

        #First write the starting position.
        p = 3
        positions[0] = startPos[0]
        positions[1] = startPos[1]
        positions[2] = startPos[2]

        #print("Positions:")
        for i in range(numLetters):
            if drawCounts[i] == 1:
                newPos = globalTransMats[i].dot(startPos)
                #newPos = startPos.dot(globalTransMats[i])
                #print("i=",i)
                #print("\tpos = ", newPos)
                positions[p] = newPos[0]
                positions[p+1] = newPos[1]
                positions[p+2] = newPos[2]
                p += 3

        return positions

    #-------------------------------------
