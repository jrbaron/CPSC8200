//Kernel functions for parts of derivation (rewriting/expanding an L-system
//  string) and interpretation (preparing for drawing / passing to OpenGL).

//---------------------------------
//Sampler object defines how an image is interpreted by the hardware.
//  This is a sampler instantiated using built-in CL constants.  A sampler object allows customization through these constants.
__constant sampler_t IMG_SAMPLER =
    CLK_NORMALIZED_COORDS_FALSE |     //Allows indexing as if the image is a multi-dimensional array.
    CLK_FILTER_NEAREST;      //Return image element nearest to the given coords.

//---------------------------------
//First part of L-system derivation/rewriting stage.
//Gather counts to aid in memory allocation for rewriting.
__kernel void countsToDerive
    (
        const __read_only image2d_t prods_img,
        const uint numProds,             //Dimensions of image as NumPy uint32
        const uint prodLen,
        __global uchar *string,          //Input string of modules to be expanded. CL uchar is np.uint8.
        __global int *letterCounts,      //Output array of needed letters to expand a char/module of the input string.
        const uint numLetters            //Of the string being derived.
    )
{
    int id, row, col, rule, prod_id;
    uchar ch;
    uint4 element;  //of the prods_img.  read_imageui() can only be used with img objects created with channel_data_type set to unsigned int 8, 16, 32
    int2 coord;

    row = get_local_id(0);  //Local image coordinates
    col = get_local_id(1);

    id = get_global_id(0);  //This should be in the range set as the "global_size" in the kernel exec call on the host side.
                            //   In this case, it is the shape of the "string" input array.

    if (id < numLetters)
    {
        ch = string[id];
      //If the character is a capital letter [A..Z]
        if ((65 <= ch) && (ch <= 90))
        {
          //Actually needed to swap x and y when having more productions than just A?
            coord.x = (ch-65) + col;           //The row (A::0, B::1, ... , Z::25)
            coord.y = 0;                       // First element of the row is the successor count.

            element = read_imageui(prods_img, IMG_SAMPLER, coord);
            letterCounts[id] = element.x;
            //letterCounts[id] = coord.x;  //Testing, looking at coord values.
        }

        else   //The char will be rewritten with itself.
            { letterCounts[id] = 1; }

    }
}

//---------------------------------
//Second part of L-system derivation/rewriting stage.
//Actually rewrite each symbol of the string based on
//  the counts of the previous step and production rules.
__kernel void rewriteString
    (
        const __read_only image2d_t prods_img,
        __global int *offsets,
        __global uchar *inputStr,
        __global uchar *outputStr,
        const uint count,
        const uint headerLen
    )
{
    int id, i, row, col, offset_ind, len;
    uchar ch;
    uint4 lenElem, elem;  //Elements from the image
    int2 coord;

    row = get_local_id(0);  //Local image coordinates
    col = get_local_id(1);
    id = get_global_id(0);  //id of inputStr

    if (id < count)
    {
        ch = inputStr[id];
        offset_ind = offsets[id];

        if ((65 <= ch) && (ch <= 90))
        {
            coord.x = (ch-65) + col;     //TODO: x and y correct, or swap?
            coord.y = 0;
            lenElem = read_imageui(prods_img, IMG_SAMPLER, coord);
            len = lenElem.x;

            for (i=0; i<len; i++)
            {
                coord.x = i+headerLen;  //+1 to exclude the header
                elem = read_imageui(prods_img, IMG_SAMPLER, coord);
                outputStr[offset_ind+i] = elem.x;
            }
        }

        else
            { outputStr[offset_ind] = ch; }

    }

}

//---------------------------------
//First part of L-system interpretation stage.
//Set local transformation matrices per letter.
//For now, 'A' symbols mean to move forward (will be a visible 'geometry object'),
//  and '+'/'-' are used for rotations by some angle (in radians).
//  Any other symbol is not terminal (though may be present in the given string;
//  depends on the result of the derivation/rewriting stage.)
//  drawCounts stores whether there will be a geometry object because of
//  the symbol.  This info is used in VBO allocation for drawing.
//Even for 2D work, float4s are used instead of float3s because apparently
//  OpenCL will use size-4 vectors instead of size-3 ones for alignment
//  purposes behind-the-scenes anyway.
__kernel void setLocalMats
    (
        __global uchar *inputStr,    //Input
        __global float4 *mats,      //Output
        __global int *drawCounts,   //Output
        const uint count,
        const float thetaRad,
        const float fwdStep
    )
{
    //Column-major 4x4 matrices


    int id, matID;
    id = get_global_id(0);  //Of the inputStr
    uchar ch;

    if (id < count)
    {
        matID = id*4;
        ch = inputStr[id];

        if (ch == 'A')
        {
            mats[matID] = (float4)(1.0,     0.0,        0.0,    0.0);
            mats[matID+1] = (float4)(0.0,   1.0,        0.0,    fwdStep);
            mats[matID+2] = (float4)(0.0,   0.0,        1.0,    0.0);
            mats[matID+3] = (float4)(0.0,   0.0,        0.0,    1.0);

            drawCounts[id] = 1;
        }

        else if (ch == '+')
        {
            mats[matID] = (float4)(cos(thetaRad), -sin(thetaRad), 0.0, 0.0);
            mats[matID+1] = (float4)(sin(thetaRad), cos(thetaRad), 0.0, 0.0);
            mats[matID+2] = (float4)(0.0, 0.0, 1.0, 0.0);
            mats[matID+3] = (float4)(0.0, 0.0, 0.0, 1.0);
            drawCounts[id] = 0;
        }

        else if (ch == '-')
        {
            mats[matID] = (float4)(cos(-thetaRad), -sin(-thetaRad), 0.0, 0.0);
            mats[matID+1] = (float4)(sin(-thetaRad), cos(-thetaRad), 0.0, 0.0);
            mats[matID+2] = (float4)(0.0, 0.0, 1.0, 0.0);
            mats[matID+3] = (float4)(0.0, 0.0, 0.0, 1.0);
            drawCounts[id] = 0;
        }

        else
        {
            mats[matID] = (float4)(1.0, 0.0, 0.0, 0.0);
            mats[matID+1] = (float4)(0.0, 1.0, 0.0, 0.0);
            mats[matID+2] = (float4)(0.0, 0.0, 1.0, 0.0);
            mats[matID+3] = (float4)(0.0, 0.0, 0.0, 1.0);
            drawCounts[id] = 0;
        }

    }
}


//---------------------------------
/*
//Second part of derivation phase.
__kernel void setPositions
   (
        __global uchar *inputStr,    //Input
        __global float *vbo,      //Output
        __global int *drawCounts,   //Output
        const uint count,
        const float thetaRad,
        const float fwdStep
    )
{
        numLetters = len(string)

        scanDrawCounts = np.cumsum(drawCounts)
        positions = np.empty( (scanDrawCounts[-1]+1)*3, dtype=np.float32 )

        #First write the starting position.
        p = 3
        positions[0] = startPos[0]
        positions[1] = startPos[1]
        positions[2] = startPos[2]

        for i in range(numLetters):
            if drawCounts[i] == 1:
                newPos = globalTransMats[i].dot(startPos)
                positions[p] = newPos[0]
                positions[p+1] = newPos[1]
                positions[p+2] = newPos[2]
                p += 3

        return positions
*/