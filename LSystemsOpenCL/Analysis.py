import matplotlib
matplotlib.use('TkAgg')

import matplotlib.pyplot as plt
import numpy as np
import _tkinter

#-------------------------------------
def runtimesChart(fractal, runType, numDepths, arrPar, arrSeq):
    plt.clf()  #Clear figure.

  #For Koch Island
    if numDepths == 8:
        depths = np.array([0,1,2,3,4,5,6,7])
    else:
        depths = np.array([0,1,2,3,4,5,6])
    axiomSize = 7
    maxProdSize = 14


    #problem size = axiom size * ({max production length}^{depth})
    #probSizes = np.zeros(7)
    #for i in range(7):
    #    probSizes[i] = axiomSize * pow(maxProdSize, depths[i])
    #    print(probSizes[i])
    probSizes = depths

   ##if displaying log2(ms)
   # par = np.ceil(arrPar)   #Round up in case of fractions in preparation for taking the log.
   # seq = np.ceil(arrSeq)   #  (Fractions are negative powers of 2, so taking log might give negatives then.)
   # par = np.log2(par)
   # seq = np.log2(seq)

    par = np.divide(arrPar, 1000)
    seq = np.divide(arrSeq, 1000)

    plt.plot(probSizes, seq, linestyle="--", color='red', label='Sequential')
    plt.plot(probSizes, par, color='blue', label='Parallel' )

    plt.title(fractal + ": " + runType + " Runtime")
    plt.xlabel("L-System Depth")
    #plt.ylabel(runType + " Runtime (log2(MS))")
    plt.ylabel(runType + " Runtime (Seconds)")

    plt.legend(loc='upper left')

    plt.savefig("charts/Chart_"+runType+"Runtimes.png", dpi=72)
    #plt.show()

#-------------------------------------
def speedupsChart(fractal, name, numDepths, arr1, arr2):
    plt.clf()  #Clear figure.
    if numDepths == 8:
        depths = np.array([0,1,2,3,4,5,6,7])
    else:
        depths = np.array([0,1,2,3,4,5,6])

    plt.plot(depths, arr1, color='red', label='Setup 1')
    plt.plot(depths, arr2, color='blue', label='Setup 2' )

    plt.plot((depths[0]-1,depths[-1]+1), (1.0, 1.0), '--')

    plt.title(fractal + ": " + name+" Speedups")
    plt.xlabel("L-System Depth")
    plt.ylabel("Speedup (Seq. Time / Par. Time)")

    plt.legend(loc='upper left')

    plt.savefig("charts/Chart_"+name+"_Speedups.png", dpi=72)


#-------------------------------------
if __name__ == "__main__":

    #Krieger
    # totalTimes_par = np.array([
    #     0.9303092957,
    #     42.1035289764,
    #     83.874464035,
    #     136.7738246918,
    #     348.4485149384,
    #     1791.2895679474,
    #     13013.876914978
    # ])
    # totalTimes_seq = np.array([
    #     0.3561973572,
    #     40.6262874603,
    #     79.532623291,
    #     147.1712589264,
    #     433.2423210144,
    #     2534.7092151642,
    #     18774.2846012115
    # ])
    #
    # #-----
    # deriveTimes_par = np.array([
    #     0.1931190491,
    #     41.1648750305,
    #     80.2040100098,
    #     114.6812438965,
    #     159.1749191284,
    #     233.4923744202,
    #     564.7473335266
    # ])
    # deriveTimes_seq = np.array([
    #     0.1904964447,
    #     40.007352829,
    #     75.1686096191,
    #     111.8588447571,
    #     158.7345600128,
    #     270.8923816681,
    #     880.8801174164
    # ])
    #
    # #-----
    # interpretTimes_par = np.array([
    #     0.7371902466,
    #     0.9386539459,
    #     3.6704540253,
    #     22.0925807953,
    #     189.2735958099,
    #     1557.7971935272,
    #     12449.1295814514
    # ])
    # interpretTimes_seq = np.array([
    #     0.1657009125,
    #     0.6189346313,
    #     4.3640136719,
    #     35.3124141693,
    #     274.5077610016,
    #     2263.8168334961,
    #     17893.4044837952
    # ])

    #EKP's
    totalTimes_par = np.array([
        6.2370300293,
        92.1139717102,
        179.8620223999,
        288.9127731323,
        606.7461967468,
        2519.9410915375,
        17778.4492969513,
        139840.867757797

    ])
    totalTimes_seq = np.array([
        0.6928443909,
        168.0707931519,
        327.9800415039,
        564.5508766174,
        729.9191951752,
        4232.400894165,
        29751.100063324,
        293584.119081497

    ])

    #-----
    deriveTimes_par = np.array([
        0.3910064697,
        86.9240760803,
        168.3928966522,
        250.7588863373,
        340.9280776978,
        463.5870456696,
        1039.1252040863,
        4906.0878753662

    ])
    deriveTimes_seq = np.array([
        0.3650188446,
        166.2559509277,
        316.8499469757,
        475.0168323517,
        354.043006897,
        518.4268951416,
        2734.3721389771,
        11561.8851184845

    ])

    #-----
    interpretTimes_par = np.array([
        5.8460235596,
        5.1898956299,
        11.4691257477,
        38.153886795,
        265.8181190491,
        2056.3540458679,
        16739.324092865,
        134934.779882431

    ])
    interpretTimes_seq = np.array([
        0.3278255463,
        1.8148422241,
        11.1300945282,
        89.5340442657,
        375.8761882782,
        3713.9739990234,
        27016.7279243469,
        282022.233963013

    ])
    #-----
    numDepths = 8
    fractal = "Koch Island"

    runtimesChart(fractal, "Total", numDepths, totalTimes_par, totalTimes_seq)
    runtimesChart(fractal,"Derivation", numDepths, deriveTimes_par, deriveTimes_seq)
    runtimesChart(fractal,"Interpretation", numDepths, interpretTimes_par, interpretTimes_seq)

    #-----
    totalSpeedups_setup1 = np.array([
        0.3828805741,
        0.9649140689,
        0.9482340568,
        1.0760191817,
        1.2433467283,
        1.4150192468,
        1.4426357898,
        1.4426357898   #Extra to match dimension
    ])
    totalSpeedups_setup2 = np.array([
        0.1110856269,
        1.8245960958,
        1.8235091384,
        1.9540530192,
        1.2030058022,
        1.6795634265,
        1.6734361679,
        2.0994157415

    ])
    #-----
    deriveSpeedups_setup1 = np.array([
        0.9864197531,
        0.971880828,
        0.9372175981,
        0.9753891827,
        0.9972334893,
        1.1601765683,
        1.5597773821,
        1.5597773821

    ])
    deriveSpeedups_setup2 = np.array([
        0.9335365854,
        1.912657096,
        1.8816111206,
        1.8943170441,
        1.0384683165,
        1.1182946115,
        2.6314173963,
        2.356640446

    ])
    #-----
    interpretSpeedups_setup1 = np.array([
        0.2247736093,
        0.6593853188,
        1.1889574537,
        1.5983833893,
        1.4503225335,
        1.4532166593,
        1.4373217313,
        1.4373217313

    ])
    interpretSpeedups_setup2 = np.array([
        0.0560766721,
        0.3496876148,
        0.9704396632,
        2.3466559186,
        1.4140352419,
        1.8060965749,
        1.6139676712,
        2.0900633195

    ])
    speedupsChart(fractal,"Total", numDepths, totalSpeedups_setup1, totalSpeedups_setup2)
    speedupsChart(fractal,"Derivation", numDepths, deriveSpeedups_setup1, deriveSpeedups_setup2)
    speedupsChart(fractal,"Interpretation", numDepths, interpretSpeedups_setup1, interpretSpeedups_setup2)
