from PyQt5.QtWidgets import QOpenGLWidget
from PyQt5.QtGui import (
    QOpenGLContext, QOpenGLShaderProgram, QOpenGLShader, QOpenGLVertexArrayObject, QOpenGLBuffer,
    QMatrix4x4, QVector3D, QVector4D
)
from PyQt5.QtCore import Qt
import array    #Python array type
import os

#Needed to find GL when using Ubuntu and NVIDIA drivers?
import ctypes.util
ctypes.CDLL(ctypes.util.find_library("GL"), mode=ctypes.RTLD_GLOBAL)


#QtWidgets.QOpenGLWidget:  Qt5 OpenGL bindings to utilize GLSL shaders.
#  (QtOpenGL.QGLWidget:  NOT what we want. Qt4 supports only the deprecated, fixed-function OpenGL.)
class OpenGLView(QOpenGLWidget):

    def __init__(self, parent, LSystemVerts=None):
        super(QOpenGLWidget, self).__init__(parent)

        #Note; OpenGL format set in Controller before the QApplication is made (especially done for OSX).

        self.parent = parent
        #self.bgColor = [0.1, 0.1, 0.12, 1.0]
        self.bgColor = [1.0, 1.0, 1.0, 1.0]
        self.vertCount = -1

        self.LSystemVerts = LSystemVerts

      #-----
        self.eyePt = QVector3D()
        self.viewPt = QVector3D()
        self.up = QVector3D()

        self.projMat = QMatrix4x4()  #Row-major
        self.modelMat = QMatrix4x4()
        self.viewMat = QMatrix4x4()
        self.fov = 60.0
        self.nearCP = 0.1    #Near clipping plane (positive because is a distance from camera)
        self.farCP = 100.0

        self.modelMat_unifLoc = -1
        self.viewMat_unifLoc = -1
        self.projMat_unifLoc = -1
        self.pos_attribLoc = -1
        self.norm_attribLoc = -1
        self.texCoord_attribLoc = -1
        self.vsName = None
        self.fsName = None

        self.vao = QOpenGLVertexArrayObject()
        self.vbo_pos = QOpenGLBuffer()
        self.vbo_norm = QOpenGLBuffer()
        self.vbo_texCoord = QOpenGLBuffer()
        self.shaderProg = None

  #----------------------------------
    def setLSystemVerts(self, verts ):
        self.LSystemVerts = verts
        self.initVertexBuffers()

    #----------------------------------
    #Must reimplement to set up the OpenGL state.
    def initializeGL(self):

        f = QOpenGLContext.currentContext().versionFunctions()
        #f = self.context().versionFunctions()
        f.initializeOpenGLFunctions()   #MacOS

        f.glClearColor( self.bgColor[0], self.bgColor[1], self.bgColor[2], self.bgColor[3] )

        self.initShaders()
        self.initVertexBuffers()

        self.eyePt = QVector3D(0.0, 0.0, -10.0)
        self.viewPt = QVector3D(0.0, 0.0, 0.0)
        self.up = QVector3D(0.0, 1.0, 0.0)
        self.viewMat.lookAt(self.eyePt, self.viewPt, QVector3D(self.up))
        self.projMat.perspective(self.fov, self.width()/self.height(), self.nearCP, self.farCP)

  #----------------------------------
    #Must reimplement to provide a perspective transformation.
    def resizeGL(self, w, h):
        self.projMat.setToIdentity()
        self.projMat.perspective(self.fov, w/h, self.nearCP, self.farCP)

  #----------------------------------
    #Must reimplement to paint the 3D scene, calling ONLY OpenGL functions (no Qt painting).
    def paintGL(self):

        f = QOpenGLContext.currentContext().versionFunctions()
        #f = self.context().versionFunctions()

        f.glClear(f.GL_DEPTH_BUFFER_BIT | f.GL_COLOR_BUFFER_BIT)
        f.glEnable(f.GL_LINE_SMOOTH)  #Enables line antialiasing.
        f.glEnable(f.GL_DEPTH_TEST)   #Enable depth-testing
        f.glDepthFunc(f.GL_LESS)      #Depth-testing inteprets a smaller value as "closer."
      #  f.glEnable(f.GL_CULL_FACE)  #TODO: Uncomment when mesh can be rendered without it.
      #  f.glCullFace(f.GL_BACK)       #Cull back faces.
      #  f.glFrontFace(f.GL_CCW)       #Counter-clock-wise vertex order determines the front of a face.

        if self.vao.isCreated():
            self.shaderProg.bind()

            self.shaderProg.setUniformValue(self.viewMat_unifLoc, self.viewMat)
            self.shaderProg.setUniformValue(self.modelMat_unifLoc, self.modelMat)
            self.shaderProg.setUniformValue(self.projMat_unifLoc, self.projMat)

            self.vao.bind()
            #f.glDrawArrays(f.GL_TRIANGLES, 0, self.vertCount)
            f.glLineWidth(5.0)
            f.glDrawArrays(f.GL_LINE_LOOP, 0, self.vertCount*3)
            self.vao.release()


  #----------------------------------
    def initShaders(self):
        self.makeCurrent()   # Called when in custom function versus Qt's.

        print("OpenGLView.initShaders()")

        self.modelMat_unifLoc = -1
        self.viewMat_unifLoc = -1
        self.projMat_unifLoc = -1
        self.vsName = "view/shaders/vs.glsl"
        self.fsName = "view/shaders/fs.glsl"

        self.shaderProg = QOpenGLShaderProgram(self)

        if self.shaderProg.addShaderFromSourceFile(QOpenGLShader.Vertex, self.vsName):
            print("\tLoaded vertex shader.")
        if self.shaderProg.addShaderFromSourceFile(QOpenGLShader.Fragment, self.fsName):
            print("\tLoaded fragment shader.")
        if self.shaderProg.link():
            print("\tLinked shader program.")
        print(self.shaderProg.log())

        self.modelMat_unifLoc = self.shaderProg.uniformLocation("modelMat")
        if self.modelMat_unifLoc == -1:
            print("\tERROR: Cannot find shader uniform 'modelMat.'")
        self.viewMat_unifLoc = self.shaderProg.uniformLocation("viewMat")
        if self.viewMat_unifLoc == -1:
            print("\tERROR: Cannot find shader uniform 'viewMat.'")
        self.projMat_unifLoc = self.shaderProg.uniformLocation("projMat")
        if self.projMat_unifLoc == -1:
            print("\tERROR: Cannot find shader uniform 'projMat.'")

        self.doneCurrent()

#----------------------------------
    #Following the setup of the shader program, VAO, and VBO calls in this:
    #https://steventaitinger.wordpress.com/2015/11/24/part-2-modern-opengl-using-qt-5-5-tutorial/
    def initVertexBuffers(self):
        print("OpenGLView.initVBOs()")
        self.makeCurrent()

        f = QOpenGLContext.currentContext().versionFunctions()

        print("\tGL context: ", QOpenGLContext.currentContext())
        self.shaderProg.bind()

        verts = []
        norms = []
        texCoords = []

        if not self.vao.isCreated():
            self.vao.create()
        if self.vao.isCreated():
            self.vao.bind()
            print("\tVAO bound.")

      #  if self.LSystemVerts is None:
      #      print("\tUsing test triangle.")
      #      verts = array.array('f', [0.5,-0.5,0, 0,0.5,0, -0.5,-0.5,0])   #CCW winding (important if CL_CULL_FACE is used).
      #      norms = array.array('f', [0,0,1, 0,0,1, 0,0,1])
      #      texCoords = array.array('f', [0,0, 1,0, 0,1])
      #      self.vertCount = int(verts.buffer_info()[1] / 3)
      #  else:
        verts = array.array('f', self.LSystemVerts)
        self.vertCount = int(verts.buffer_info()[1] / 3)
        norms = array.array('f', (self.vertCount*[0,0,1]))
        texCoords = array.array('f', self.vertCount*[0,0])

        normCount = norms.buffer_info()[1] / 3
        tcCount = texCoords.buffer_info()[1] / 2
        print("\tvert, norm, tc counts:", self.vertCount, normCount, tcCount)

        #---------
        self.vbo_pos.create()
        self.vbo_pos.setUsagePattern(QOpenGLBuffer.DynamicDraw)
        self.vbo_pos.bind()

      #allocate(Data as bytes, Num of items * Byte size of items)
        self.vbo_pos.allocate(verts, self.vertCount*3*verts.itemsize)
        self.pos_attribLoc = self.shaderProg.attributeLocation("vertexPosition")
        self.shaderProg.enableAttributeArray(self.pos_attribLoc)
        self.shaderProg.setAttributeBuffer(self.pos_attribLoc, f.GL_FLOAT, 0, 3)

        #---------
        self.vbo_norm.create()
        self.vbo_norm.bind()
        #self.vbo_n.allocate(test_norms, 3 * count * sizeof(GLfloat))
        self.vbo_norm.allocate(norms, normCount*3*norms.itemsize)
        self.norm_attribLoc = self.shaderProg.attributeLocation("vertexNormal")
        self.shaderProg.enableAttributeArray(self.norm_attribLoc)
        self.shaderProg.setAttributeBuffer(self.norm_attribLoc, f.GL_FLOAT, 0, 3)

        #---------
        self.vbo_texCoord.create()
        self.vbo_texCoord.bind()
        #self.vbo_texCoord.allocate(test_tc, 2 * count * sizeof(GLfloat))
        self.vbo_texCoord.allocate(texCoords, tcCount*2*texCoords.itemsize)
        self.texCoord_attribLoc = self.shaderProg.attributeLocation("vertexTexcoord")
        self.shaderProg.enableAttributeArray(self.texCoord_attribLoc)
        self.shaderProg.setAttributeBuffer(self.texCoord_attribLoc, f.GL_FLOAT, 0, 2)

        #---------
        self.vbo_pos.release()
        self.vbo_norm.release()
        self.vbo_texCoord.release()
        self.vao.release()
        self.shaderProg.release()

        self.doneCurrent()

  #----------------------------------
    def reset(self):
        print("OpenGLView.reset()")
        self.initVertexBuffers()   #Do not call initializeGL() again; that's done only once.
