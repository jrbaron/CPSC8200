from PyQt5.QtWidgets import QMainWindow, QDesktopWidget, QVBoxLayout, QPushButton
from OpenGLView import OpenGLView
import numpy as np
import datetime
from time import time

class MainWindow(QMainWindow):
    def __init__(self, LGen, logfile):
        super(MainWindow, self).__init__()
        self.logfile = logfile
        self.logfile.write("L-System Generator Logfile")
        self.logfile.write("\nCreated on " + str(datetime.datetime.now()) )
        self.logfile.write("\nAuthor: Jessica Baron")

        self.generator = LGen
        positions = self.initLGenerator()

        self.setWindowTitle("OpenCL L-Systems")
        self.resize(QDesktopWidget().availableGeometry(self).size() * 0.9)
        self.resize(self.width()*0.8, self.height())

        self.view = OpenGLView(self, positions)
        self.setCentralWidget(self.view)

        self.show()

    #--------------------
    def initLGenerator(self):
        self.logfile.write("\n\nL-System Attributes:")
        self.logfile.write("\n\tFractal Name: \t\t\t\t" + self.generator.name)
        self.logfile.write("\n\tDerivation Depth: \t\t\t" + str(self.generator.depth))
        self.logfile.write("\n\tLine segment length: \t\t" + str(self.generator.fwdStep))
        self.logfile.write("\n\tRotation angle (radians): \t" + str(self.generator.rotRad))
        self.logfile.write("\n\tUsing OpenCL kernels? \t\t" + str(self.generator.useOpenCL))

        axiom = None
        positions = []

        if self.generator.name == "KochIsland":
          #Koch Island: A -> A+A-A-AA+A+A-A
            axiom = 'A+A+A+A'
            self.generator.addProduction("A+A-A-AA+A+A-A")
            #thetaRad = np.pi / 2.0

        elif self.generator.name == "KochSquare":
          #Koch curve square (Prusinkiewicz 1989, pg 16)
            axiom = 'A+A+A+A'
            self.generator.addProduction("AA+A+A+A+AA")  #A -> AA+A+A+A+AA
            #thetaRad = np.pi / 2.0

      #Sierpinski arrowhead (Prusinkiewicz 1989, pg 19)
        # self.generator.addProduction("A")  #A -> A
        # self.generator.addProduction("CA+BA+C")  #B -> CA+BA+C
        # self.generator.addProduction("BA-CA-B")  #C -> BA-CA-B
        # thetaRad = np.pi / 3.0
        # st = self.generator.expandString('CA', 1)


        if axiom is not None:
            self.logfile.write("\n\tAxiom String: \t\t\t\t" + axiom)
            self.logfile.write("\n")

            runtime = time()
            st = self.generator.expandString(axiom, self.logfile)
            deriveTime = time() - runtime

            #print("Axiom: ", axiom)
            #print("Derived String: ", st)

            #fwdStep = 0.05
            startPos = np.array([0.0, 0.0, 0.0, 1.0])
            ##startPos = np.array([1.0, -1.0, 0.0, 1.0])  #Funky if not origin!
            #positions = self.generator.makeVBO(st, startPos, thetaRad, fwdStep)

            runtime = time()
            positions = self.generator.makeVBO(self.logfile, st, startPos)
            interpretTime = time() - runtime

            self.logfile.write("\n\nTotal Runtimes (Milliseconds):")
            self.logfile.write("\n\tDerivation: \t\t" + str(deriveTime*1000))
            self.logfile.write("\n\tInterpretation: \t" + str(interpretTime*1000))

        else:
            print("MainWindow.initGen(): ACHTUNG! No axiom set. Check fractal name.")


        return positions

    #--------------------
